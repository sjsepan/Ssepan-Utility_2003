﻿using System;

namespace Ssepan.Utility
{
    public /*static*/ class StringExtensions
    {
        /// <summary>
        /// Perform string.Contains(value) with case-sensitivity explicitly on or off.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="caseInsensitive"></param>
        /// <returns></returns>
        public static Boolean Contains(String source, String value, Boolean caseInsensitive)
        {

            if (caseInsensitive)
            {
                Int32 results = source.IndexOf(value);
                return results == -1 ? false : true;
            }
            else
            {
                return Contains(source, value, caseInsensitive);
            }

        }
        
        public static Boolean IsNullOrEmpty(String value)
        {
			return ((value == null) || (value == String.Empty));
        }
    }
}
