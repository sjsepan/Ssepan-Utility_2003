﻿using System;
using System.Collections;
using System.Diagnostics;
//using System.Collections.Generic;
//using System.Linq;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Ssepan.Utility
{
    /// <summary>
    /// http://www.devexpertise.com/category/net/linq/
    /// </summary>
    public /*static*/ class LikeOperator
    {
        /// <summary>
        /// Ex: var results = (from v in values where v.Like("*a*a*") select v);
        /// </summary>
        /// <param name="value"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public static Boolean Like(String value, String term)
        {
            Boolean returnValue = false;
            Regex regex = null;

            try
            {
                regex = new Regex(String.Format("^{0}$", term.Replace("*", ".*")), RegexOptions.IgnoreCase);
				returnValue = regex.IsMatch(value);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Ex: var results = values.Like("*a*a*");
        /// </summary>
        /// <param name="source"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static ArrayList Like(ArrayList source, String expression)
        {
            ArrayList returnValue = null;

            try
            {
				//returnValue = (from s in source where s.Like(expression) select s);
				foreach (String s in source)
				{
					if (Like(s, expression))
					{
						returnValue.Add(s);
					}
				}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
                throw;
            }
            return returnValue;
        }
    }
}
