# readme.md - README for Ssepan.Utility (and Ssepan.* Libraries) v6.0

## About

Common library of utility functions for C# applications
Ssepan.Utility back-ported to Visual C# 2003 and .Net 1.1. Note: Going from later version requires giving up several features.
~ No Action type; used delegates
~ No generics; used overloads
~ No LINQ; used for and if/else
~ Different types for menu and toolbar; used ImageList for toolbar images
~ No 'default' keyword; used explicit nulls, enums, or values

### Purpose

To encapsulate common functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### Setup

~Application was developed on Windows Server 2003 pre-SP1, Visual C# 2003 and .Net Framework 1.1. 

### History

6.0:
~initial port from Ssepan.Utility.Mono;

### Fixes

~

### Known Issues

~  

### Possible Enhancements

~

Steve Sepan
sjsepan@yahoo.com
9/22/2022
