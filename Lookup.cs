﻿//#define USEEVENTLOG

using System;
using System.Collections;
using System.Diagnostics;
//using System.Linq;
using System.Reflection;

namespace Ssepan.Utility
{
    public class Lookup
    {
        public Lookup()
        {
        }

        public Lookup
        (
            String value,
            String text
        ) :
            this()
        {
            Value = value;
            Text = text;
        }

        private String _Value = null;
        public String Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private String _Text = null;
        public String Text
        {
            get { return _Text; }
            set { _Text = value; }
        }


		public static ArrayList GetEnumLookup(Type tEnum)
		{
			return GetEnumLookup(tEnum, false);
		}
	
		public static ArrayList GetEnumLookup(Type tEnum, Boolean addNotSelectedItem)
        {
            ArrayList returnValue = null;

            try
            {
//                returnValue = (from Enum value in Enum.GetValues(typeof(tEnum))
//                               select new Lookup(value.ToString(), value.ToString())).ToList();
				foreach (Enum value in Enum.GetValues(tEnum))
				{
					returnValue.Add(new Lookup(value.ToString(), value.ToString()));
				}
                if (addNotSelectedItem)
                {
                    returnValue.Insert(0, new Lookup("", "(Not Selected)"));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), EventLogEntryType.Error);
            }
            return returnValue;
        }
    }
}
